AIRAC: 2021/04/22
  WHITE WALTHAM - Elevation changed

AIRAC: 2021/03/25
  No change

Non-AIRAC:
  DONCASTER LOA - Split into separate boxes
  WORKSOP CORRIDOR LOA - Add
  RUGELEY CHM - Obstacle removed

AIRAC: 2021/02/25
  BORDERS CTA - Add, replacing  various airways
  YORKSIRE CTA - Add, replacing  various airways
  LINTON-ON-OUZE - Delete ATZ and MATZ
  SHAWBURY - New gliding site

AIRAC: 2021/01/28
  BOSCOMBE DOWN ATZ - Coordinates changed
  COLERNE ATZ - Coordinates changed
  COSFORD ATZ - Coordinates changed
  FAIRFORD ATZ - Coordinates changed
  HALTON ATZ - Coordinates changed
  LEUCHARS ATZ - Coordinates changed
  LOSSIEMOUTH ATZ - Coordinates changed
  MERRYFIELD ATZ - Coordinates changed
  MIDDLE WALLOP ATZ - Coordinates changed
  NETHERAVON ATZ - Coordinates changed
  SCAMPTON ATZ - Coordinates changed
  SHAWBURY ATZ - Coordinates changed
  TERNHILL ATZ - Coordinates changed
  TOPCLIFFE ATZ - Coordinates changed
  WATTISHAM ATZ - Coordinates changed
  WOODVALE ATZ - Coordinates changed
  BLACKBuSHE ATZ - Coordinates changed
  BARTON ATZ - Coordinates changed
  YEOVIL ATZ - Coordinates changed

AIRAC: 2020/12/31
  BOURNEMOUTH - 8.33 kHz frequency
  WELSHPOOL ATZ - Amend elevation

AIRAC: 2020/12/03
  HAVERFORDWEST - Frequency change

Non-AIRAC:
  BLACKPOOL - Freqeuncy change (missed from earlier AIRAC)
  TRURO - Remove radio frequency (missed from earlier AIRAC)
  WEST WALES ATZ - Frequency change (missed from earlier AIRAC)

AIRAC: 2020/11/05
  BICESTER - Reduce height (no winch)
  BOOKER - Modify elevation
  OXFORD ATZ - Modify elevation

AIRAC: 2020/10/08
  LYDD ATZ - Modify coordinates

Non-AIRAC:
  LASHENDEN RA(T)
  DUXFORD RA(T)
  TOPCLIFFE RA(T)

AIRAC: 2020/09/10
  BIGGIN HILL ATZ - Update upper limit
  MISK HILL UL - Delete
  TOPCLIFFE MATZ - Remove stub

Non-AIRAC:
  Add offshore danger areas
  Gliding sites - Update frequencies
  BICESTER - Re-instate gliding club
  D026 LULWORTH - Remove NOTAM activated upper part
  D110 BRAUNTON BURROWS - NOTAM activated
  LEE-ON-SOLENT - Remove gliding site

AIRAC: 2020/08/13
  BIGGIN HILL - Update elevation
  EXETER - Update frequency
  Obstacle updates

Non-AIRAC:
  BICESTER - Delete
  RAF CRANWELL RA(T)
  SILVERSTONE RA(T)
  OLD WARDEN RA(T)
  EAST MIDLANDS (CAMPHILL) LOA - Update vertical limits
  POCKLINGTON - Add frequency
  SOUTHEND CORRIDOR - Reclassify and tweak coordinates

AIRAC: 2020/07/16
  NEWQUAY - Elevation changed
  OBAN - Elevation changed
  OLD WARDEN - Elevation changed

Non-AIRAC:
  WALES (COVID-19) - Add RA(T) prohibited area

AIRAC: 2020/06/18
  BLACKBUSHE - Remove Farnbourgh cut-out

Non-AIRAC:
  Correct/tidy NOTAM activated danger areas
  DUNSTABLE COMPS LOA - Add
  WOBURN (NO LANDING) - Add

AIRAC: 2020/05/21
  OLD SARUM ATZ - Delete
  TOPCLIFFE - Radio service now from Leeming
