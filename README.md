Airspace data for the United Kingdom
------------------------------------

Airspace data in YAIXM format (see https://gitlab.com/ahsparrow/yaixm).

The data is split into four files:

1. airspace.yaml - Airspace data from the UK Aeronautical Information Package

2. loa.yaml - BGA Letters of Agreement (see https://members.gliding.co.uk/library/loas)

3. obstacle.yaml - Obstacle data from the data file listed in section ENR 5.4
   of the UK AIP.

4. rat.yaml - RA(T) data from mauve AICs
